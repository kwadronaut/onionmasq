package org.torproject.artitoyvpn.utils;

import android.content.ClipData;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

import org.torproject.artitoyvpn.R;

import java.util.Timer;
import java.util.TimerTask;

public class Utils {

    public static boolean isRunningOnMainThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }


    public static void runWithDelay(long delay, Runnable runnable) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runnable.run();
            }
        }, delay);
    }

    public static boolean isMainThread() {
        return Looper.myLooper() != null && Looper.myLooper() == Looper.getMainLooper();
    }

    public static void runOnMain(final @NonNull Runnable runnable) {
        if (isMainThread()) runnable.run();
        else {
            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }

    public static void writeTextToClipboard(@NonNull Context context, @NonNull String text) {
        android.content.ClipboardManager clipboard =
                (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(context.getString(R.string.app_name), text);
        clipboard.setPrimaryClip(clip);
    }
}
