package org.torproject.artitoyvpn.ui.appListing;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import org.torproject.artitoyvpn.vpn.VpnStatusObservable;
import org.torproject.onionmasq.events.RelayDetails;
import org.torproject.onionmasq.circuit.Circuit;

import java.util.ArrayList;

public class CircuitDialogViewModel extends AndroidViewModel {
    public String circuitDetails = "";
    private final int appUID;

    public CircuitDialogViewModel(@NonNull Application application, int appUID) {
        super(application);
        this.appUID = appUID;
        init();
    }

    public void init() {
        ArrayList<Circuit> circuits = VpnStatusObservable.getCircuitListForUid(appUID);
        StringBuilder details = new StringBuilder();
        int i = 0;
        for (Circuit circuit : circuits) {
            i++;
            details.append("~~~~~ CIRCUIT ").append(i).append("~~~~~\n");
            details.append("    ").append(circuit.getDestinationDomain()).append("\n");
            int k = 0;
            for (RelayDetails relay : circuit.getRelayDetails()) {
                k++;
                details.append("  relay ").append(k).append(":\n");
                for (String address : relay.addresses) {
                    details.append("    ").append(address).append("\n");
                }
            }
        }

        if (details.length() == 0) {
            details.append("Currently no tor circuits.");
        }

        circuitDetails = details.toString();
    }

}