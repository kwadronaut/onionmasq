#!/bin/bash

# If you want to speed up compilation, you can run this script with your
# architecture as an argument:
#
#     ./ndk-make.sh arm64-v8a
#
# Possible values are armeabi-v7a, arm64-v8a, x86 and x86_64.
# You should be able to find out your architecture by running:
#
#     adb shell uname -m
#
# Or you just guess your phone's architecture to be arm64-v8a and if you
# guessed wrongly, a warning message will pop up inside the app, telling
# you what the correct one is.
#
# The values in the following lines mean the same:
#
# armeabi-v7a, armv7 and arm
# arm64-v8a, aarch64 and arm64
# x86 and i686
# (there are no synonyms for x86_64)
#
#

RED='\033[0;31m'
NC='\033[0m'

function quit {
    echo -e "${RED}$1${NC}"
    exit 1
}

set -e
echo "starting time: `date`"

EXPECTED_NDK_VERSION="25.2.9519653"

: "${ANDROID_NDK_ROOT:=$ANDROID_NDK_HOME}"
: "${ANDROID_NDK_ROOT:=$ANDROID_NDK}"
if test -z "$ANDROID_NDK_ROOT"; then
    quit "Please set \$ANDROID_NDK environment variable"
fi

if [[ ! "${ANDROID_NDK_ROOT##*/}" == $EXPECTED_NDK_VERSION ]]; then
    quit "Error: Please set ANDROID_NDK to the NDK file path of version $EXPECTED_NDK_VERSION"
fi

echo Setting CARGO_TARGET environment variables.

if test -z "$NDK_HOST_TAG"; then
    KERNEL="$(uname -s | tr '[:upper:]' '[:lower:]')"
    ARCH="$(uname -m)"
    NDK_HOST_TAG="$KERNEL-$ARCH"
    echo $ARCH
    echo $KERNEL
    if [[ ${KERNEL} == "darwin" ]]; then
        TOOLCHAIN_HOST="apple-darwin"
        DATE_SPECIFIER=""
    elif [[ ${KERNEL} == "linux" ]]; then
        TOOLCHAIN_HOST=$KERNEL-gnu
        DATE_SPECIFIER="unknown-"
    else
        echo "TODO: adapt toolchain host variable for Windows"
    fi
fi

TOOLCHAIN="$ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/$NDK_HOST_TAG"
ROOT_DIR=$PWD
BUILD_DIR=$ROOT_DIR/build
CARGO_TARGET_DIR=$ROOT_DIR/target
CARGO_TOOLCHAIN=+stable-$ARCH-${DATE_SPECIFIER}${TOOLCHAIN_HOST}

LLVM_PREBUILD=$TOOLCHAIN/bin
export PATH=$LLVM_PREBUILD:$PATH

# Check if the argument is a correct architecture:
if test $1 && echo "armeabi-v7a arm64-v8a x86 x86_64" | grep -vwq $1; then
    quit "Architecture '$1' not known, possible values are armeabi-v7a, arm64-v8a, x86 and x86_64."
fi

# fix build on MacOS Catalina
unset CPATH

if test -z $1; then
  echo Full build

  # According to 1.45.0 changelog in https://github.com/rust-lang/rust/blob/master/RELEASES.md,
  # "The recommended way to control LTO is with Cargo profiles, either in Cargo.toml or .cargo/config, or by setting CARGO_PROFILE_<name>_LTO in the environment."
  export CARGO_PROFILE_RELEASE_LTO=on
  RELEASE="release"
  RELEASEFLAG="--release"
else
  echo Fast, partial debug build. DO NOT UPLOAD THE APK ANYWHERE.

  RELEASE="debug"
  RELEASEFLAG=
fi

if test -z "$VERBOSE"; then
  FEATURES_FLAG="--features pluggable-transports"
else
  echo "VERBOSE is set, building with more logging. DO NOT UPLOAD THE APK ANYWHERE."
  FEATURES_FLAG="--features pluggable-transports,verbose"
fi

# Lint checking
cargo fmt --check --package onionmasq-mobile || quit "Please fix your formatting"

if test -z $1 || test $1 = armeabi-v7a; then
    echo "-- cross compiling to armv7-linux-androideabi (arm) --"
    if [[ ! -f ${BUILD_DIR}/armeabi-v7a ]]; then mkdir -p $BUILD_DIR/armeabi-v7a; fi
    # We set this special environment variable for the onionmasq-pt-wrapper build script to find later.
    export ONIONMASQ_PT_LIB_LOCATION="${BUILD_DIR}/armeabi-v7a/libonionmasq-pt-bindings.so"
    cargo ndk --target armv7-linux-androideabi build $RELEASEFLAG $FEATURES_FLAG --lib
    cp $CARGO_TARGET_DIR/armv7-linux-androideabi/$RELEASE/libonionmasq_mobile.so $BUILD_DIR/armeabi-v7a
fi

if test -z $1 || test $1 = arm64-v8a; then
    echo "-- cross compiling to aarch64-linux-android (arm64) --"
    if [[ ! -f ${BUILD_DIR}/arm64-v8a ]]; then mkdir -p $BUILD_DIR/arm64-v8a; fi
    export ONIONMASQ_PT_LIB_LOCATION="${BUILD_DIR}/arm64-v8a/libonionmasq-pt-bindings.so"
    cargo ndk --target aarch64-linux-android build $RELEASEFLAG $FEATURES_FLAG --lib
    cp $CARGO_TARGET_DIR/aarch64-linux-android/$RELEASE/libonionmasq_mobile.so $BUILD_DIR/arm64-v8a
fi

if test -z $1 || test $1 = x86; then
    echo "-- cross compiling to i686-linux-android (x86) --"
    if [[ ! -f ${BUILD_DIR}/x86 ]]; then mkdir -p $BUILD_DIR/x86; fi
    export ONIONMASQ_PT_LIB_LOCATION="${BUILD_DIR}/x86/libonionmasq-pt-bindings.so"
    cargo ndk --target i686-linux-android build $RELEASEFLAG $FEATURES_FLAG --lib
    cp $CARGO_TARGET_DIR/i686-linux-android/$RELEASE/libonionmasq_mobile.so $BUILD_DIR/x86
fi

if test -z $1 || test $1 = x86_64; then
    echo "-- cross compiling to x86_64-linux-android (x86_64) --"
    if [[ ! -f ${BUILD_DIR}/x86_64 ]]; then mkdir -p $BUILD_DIR/x86_64; fi
    export ONIONMASQ_PT_LIB_LOCATION="${BUILD_DIR}/x86_64/libonionmasq-pt-bindings.so"
    cargo ndk --target x86_64-linux-android build $RELEASEFLAG $FEATURES_FLAG --lib
    cp $CARGO_TARGET_DIR/x86_64-linux-android/$RELEASE/libonionmasq_mobile.so $BUILD_DIR/x86_64
fi
