use anyhow::Context;
use clap::{Arg, Command};
use log::{info, trace, warn};
use onion_tunnel::scaffolding::{ConnectionDetails, FailedConnectionDetails};
use onion_tunnel::{CountryCode, IpEndpoint, OnionTunnel, TunnelScaffolding};
use simple_proc_net::ProcNetEntry;
use std::ffi::c_void;
use std::io;
use std::mem::size_of;
use std::net::SocketAddr;
use std::os::fd::AsRawFd;
use std::os::fd::RawFd;
use std::str::FromStr;
use tokio::net::TcpSocket;
use tracing_subscriber::FmtSubscriber;

struct LinuxScaffolding {
    cc: Option<CountryCode>,
    can_mark: bool,
    log_connections: bool,
}

impl LinuxScaffolding {
    /// The fwmark to set on arti connections (so policy routing can avoid them being routed
    /// recursively back into the tunnel).
    ///
    /// This is just a randomly generated set of 2 bytes that hopefully won't conflict with
    /// anything else.
    pub const FWMARK: libc::c_int = 0xc185;

    /// Mark the provided socket file descriptor with the `FWMARK`.
    #[cfg(target_os = "linux")]
    fn mark_fd(fd: RawFd) -> io::Result<()> {
        let ret = unsafe {
            libc::setsockopt(
                fd,
                libc::SOL_SOCKET,
                libc::SO_MARK,
                &Self::FWMARK as *const libc::c_int as *const c_void,
                size_of::<libc::c_int>() as _,
            )
        };
        if ret != 0 {
            Err(io::Error::last_os_error())
        } else {
            Ok(())
        }
    }
}

impl TunnelScaffolding for LinuxScaffolding {
    fn protect(&self, fd: RawFd, _: &SocketAddr) -> io::Result<()> {
        #[cfg(target_os = "linux")]
        if self.can_mark {
            Self::mark_fd(fd)?;
        }
        Ok(())
    }

    fn locate(&self, _: IpEndpoint, _: IpEndpoint, _: u64) -> Option<CountryCode> {
        self.cc
    }

    fn on_bootstrapped(&self) {
        info!("Connection to Tor complete!");
    }

    fn on_established(&self, details: ConnectionDetails<'_>) {
        if self.log_connections {
            let exit_identity = details
                .circuit_relays()
                .last()
                .map(|x| {
                    let cc = match x.country_code {
                        Some(v) => v.to_string(),
                        None => "??".into(),
                    };
                    if let Some(i) = x.ed_identity {
                        format!("{} ({})", i, cc)
                    } else if let Some(i) = x.rsa_identity {
                        format!("{} ({})", i, cc)
                    } else {
                        "???".into()
                    }
                })
                .unwrap_or_else(|| "???".into());

            info!(
                "New connection to '{}' (uid {}) via exit {}",
                details.tor_dst, details.isolation_key, exit_identity
            );
        }
    }

    fn on_arti_failure(&self, details: FailedConnectionDetails) {
        if self.log_connections {
            warn!(
                "Failed to connect to '{}': {}",
                details.tor_dst, details.error
            );
        }
    }

    fn isolate(&self, src: IpEndpoint, dst: IpEndpoint, ip_proto: u8) -> io::Result<u64> {
        let iter = match ip_proto {
            6 => ProcNetEntry::tcp4()?.chain(ProcNetEntry::tcp6()?),
            17 => ProcNetEntry::udp4()?.chain(ProcNetEntry::udp6()?),
            x => panic!("unknown ip_proto {x}"),
        };
        for entry in iter {
            match entry? {
                Ok(v) => {
                    if IpEndpoint::from(v.local_addr) == src
                        && IpEndpoint::from(v.remote_addr) == dst
                    {
                        trace!("isolated {src} -> {dst} proto {ip_proto} as {}", v.uid);
                        return Ok(v.uid as u64);
                    }
                }
                Err(e) => {
                    warn!("Failed to parse /proc line: {}", e.offending_line);
                    warn!("Error encountered: {}", e.error);
                }
            }
        }
        warn!("no isolation found for {src} -> {dst} proto {ip_proto}");
        Ok(0)
    }
}

const ENV_FILTER_VERBOSE: &str =
    "info,smoltcp=debug,onion_tunnel=trace,arti_client=debug,tor_chanmgr=debug,tor_proto=debug";
const ENV_FILTER_TAME: &str = "info";

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let matches = Command::new("onionmasq")
        .version(env!("CARGO_PKG_VERSION"))
        .author("eta <eta@torproject.org>")
        .about("A magical TUN device that feeds traffic via Tor.")
        .arg(
            Arg::new("tun-device")
                .short('d')
                .long("device")
                .value_name("DEVICE")
                .default_value("onion0")
                .help(
                    "Name of a TUN device to use. Will attempt to create one if it doesn't exist.",
                ),
        )
        .arg(
            Arg::new("country-code")
                .short('c')
                .long("country-code")
                .value_name("DE|NL|etc")
                .help("Make traffic come from exit nodes in this country (ISO 3166-1 alpha-2 country code)"),
        )
        /* TODO(eta): make this a thing
        .arg(Arg::new("whole-system")
            .short('s')
            .long("whole-system")
            .help("Make all system traffic go through the onionmasq TUN device. Off by default.")
            .action(clap::ArgAction::SetTrue)
        )
         */
        .arg(Arg::new("debug")
            .long("debug")
            .help("Enable detailed debug logging. Off by default.")
            .action(clap::ArgAction::SetTrue)
        )
        .arg(Arg::new("verbose")
            .short('v')
            .long("verbose")
            .help("Print information about successful and failed connections. Off by default.")
            .action(clap::ArgAction::SetTrue))
        .get_matches();

    let filter = if matches.get_flag("debug") {
        ENV_FILTER_VERBOSE
    } else {
        ENV_FILTER_TAME
    };

    let log_connections = matches.get_flag("verbose");

    let country_code = matches
        .get_one("country-code")
        .map(|x: &String| CountryCode::from_str(x))
        .transpose()
        .context("invalid country code provided")?;

    let tun_device: &String = matches.get_one("tun-device").unwrap();

    FmtSubscriber::builder().with_env_filter(filter).init();

    info!(
        "Starting onionmasq {} on device '{}'...",
        env!("CARGO_PKG_VERSION"),
        tun_device
    );

    // Check whether we can call setsockopt(). If we can't, don't bother doing so in future, since
    // we probably don't have the capabilities; just print an explanatory warning instead.
    #[cfg(target_os = "linux")]
    let dummy_socket = TcpSocket::new_v4()?;
    #[cfg(target_os = "linux")]
    let can_mark = match LinuxScaffolding::mark_fd(dummy_socket.as_raw_fd()) {
        Ok(_) => {
            info!(
                "Outgoing connections (to the Tor network) will use fwmark {:#x}.",
                LinuxScaffolding::FWMARK
            );
            true
        }
        Err(e) => {
            warn!("Calling setsockopt() failed: {e}");
            warn!("Setting fwmarks on outgoing sockets will be disabled.");
            warn!("Make the onionmasq binary CAP_NET_ADMIN to fix this problem.");
            false
        }
    };

    #[cfg(not(target_os = "linux"))]
    let can_mark = false;

    let scaffolding = LinuxScaffolding {
        can_mark,
        cc: country_code,
        log_connections,
    };

    #[cfg(target_os = "linux")]
    let mut onion_tunnel = OnionTunnel::new(scaffolding, "onion0", Default::default()).await?;

    info!("Connecting to Tor...");

    #[cfg(target_os = "linux")]
    tokio::select! {
        _ = onion_tunnel.run() => (),
        _ = tokio::signal::ctrl_c() => {
            info!("Received ^C; stopping tunnel.");
        }
    };

    Ok(())
}
